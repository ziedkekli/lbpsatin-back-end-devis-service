package com.LBPSatin.devisservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class DevisServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevisServiceApplication.class, args);
	}

}
