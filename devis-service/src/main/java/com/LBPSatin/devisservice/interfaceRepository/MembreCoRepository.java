package com.LBPSatin.devisservice.interfaceRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.LBPSatin.devisservice.entity.MembreCo;

public interface MembreCoRepository extends JpaRepository<MembreCo, Long> {

}
