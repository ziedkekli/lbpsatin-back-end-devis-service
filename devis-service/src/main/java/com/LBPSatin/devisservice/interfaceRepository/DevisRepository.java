package com.LBPSatin.devisservice.interfaceRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.LBPSatin.devisservice.entity.Devis;

public interface DevisRepository extends JpaRepository<Devis, Long>{

}
