package com.LBPSatin.devisservice.interfaceRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.LBPSatin.devisservice.entity.Prospect;

@Repository
public interface ProspectRepository extends JpaRepository<Prospect, Long> {

}
