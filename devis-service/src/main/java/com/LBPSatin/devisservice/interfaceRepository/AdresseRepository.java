package com.LBPSatin.devisservice.interfaceRepository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.LBPSatin.devisservice.entity.Adresse;

public interface AdresseRepository extends JpaRepository<Adresse, Long> {

}
