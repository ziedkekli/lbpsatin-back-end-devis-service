package com.LBPSatin.devisservice.interfaceRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.LBPSatin.devisservice.entity.ContratBrouillon;

@Repository
public interface ContratBrouillonRepository extends JpaRepository<ContratBrouillon, Long> {

}
