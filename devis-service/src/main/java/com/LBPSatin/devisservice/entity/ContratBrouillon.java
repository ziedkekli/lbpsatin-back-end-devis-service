package com.LBPSatin.devisservice.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.LBPSatin.devisservice.enumerates.EtatContrat;
import com.LBPSatin.devisservice.enumerates.TypeContratBrouillon;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContratBrouillon {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="contratBrouillonid_generator")
	@SequenceGenerator(name = "contratBrouillonid_generator",initialValue=1,sequenceName="contratBrouillonid_seq")
	@Column(name = "id_contratBrouillon", unique=true, nullable=false)
	private Long idContratBrouillon;
	
	@Column(name = "code_contrat")
    private Long codeContrat;
	
	@Column(name = "num_contrat")
	private Long numContrat;
	
	@Column(name = "type_contrat_brouillon")
	private TypeContratBrouillon typeContratBrouillon;
	
	@Column(name = "etat_contrat_brouillon")
	private EtatContrat etatContratBrouillon;
	
	// date d'effet à partir de demain ou bien une date choisie par l'utilisateur
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_souscription")
	private Date dateSouscription;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_creation")
	private Date dateCreation;
	
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_maj")
	private Date dateMaj;
	
	@OneToOne(mappedBy = "contratBrouillon")
    private Devis devis;
	
	@OneToMany(mappedBy = "contratBrouillon", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<MembreCo> membreCos;
	
}
