package com.LBPSatin.devisservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Adresse {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="adresseid_generator")
	@SequenceGenerator(name = "adresseid_generator",initialValue=1,sequenceName="adresseid_seq")
	@Column(name = "id_adresse", unique=true, nullable=false)
	private Long idAdresse;
	
	@Column(name = "adresse")
    private String adresse;
	
	@Column(name = "adresse_comp")
	private String adresseComp;
	
	@Column(name = "code_postal")
	private String codePostal;
	
	@Column(name = "ville")
	private String ville;
	
	@Column(name = "pays")
	private String pays;
	
	@OneToOne(mappedBy = "adresse")
    private Prospect prospect;

}
