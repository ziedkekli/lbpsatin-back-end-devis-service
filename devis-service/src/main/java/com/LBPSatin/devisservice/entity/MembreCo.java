package com.LBPSatin.devisservice.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.LBPSatin.devisservice.enumerates.EtatMembreCo;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MembreCo {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="membreCoid_generator")
	@SequenceGenerator(name = "membreCoid_generator",initialValue=1,sequenceName="membreCoid_seq")
	@Column(name = "id_membreCo", unique=true, nullable=false)
	private Long idMembreCo;
	
	@Column(name = "etat_membreCo")
	private EtatMembreCo etatMembreCo;
	
	@Column(name = "position")
	private int position;
	
	@Column(name = "souscripteur")
	private int souscripteur;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_creation")
	private Date dateCreation;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_modification")
	private Date dateModification;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "id_contratBrouillon", nullable=false)
	@JsonIgnore
	private ContratBrouillon contratBrouillon;
	
	@OneToOne(mappedBy = "membreCo")
    private Prospect prospect;
}
