package com.LBPSatin.devisservice.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.LBPSatin.devisservice.enumerates.EtatProspect;
import com.LBPSatin.devisservice.enumerates.NatureProspect;
import com.LBPSatin.devisservice.enumerates.Responsable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Prospect {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="prospectid_generator")
	@SequenceGenerator(name = "prospectid_generator",initialValue=1,sequenceName="prospectsid_seq")
	@Column(name = "id_contratBrouillon", unique=true, nullable=false)
	private Long idProspect;
	
	@Column(name = "nom")
    private String nom;
	
	@Column(name = "prenom")
    private String prenom;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_naissance")
	private Date dateNaissance;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_debut_contrat")
	private Date dateDebutContrat;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_creation")
	private Date dateCreation;

	@Column(name = "nbr_enfant")
	private int nbrEnfant;
	
	@Column(name = "nbr_adulte")
	private int nbrAdulte;
	
	@Column(name = "responsable")
	private Responsable responsable;
	
	@Column(name = "nature")
	private NatureProspect nature;
	
	@Column(name = "etat_prospect")
	private EtatProspect etatProspect;
	
	@Column(name = "consentement_refus")
	private boolean consentementRefus;
	
	// clé étrangére sur table Adresse
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_adresse", referencedColumnName = "id_adresse")
    private Adresse adresse;
	
	// clé étrangére sur table MembreCo
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_membreCo", referencedColumnName = "id_membreCo")
    private MembreCo membreCo;

}
