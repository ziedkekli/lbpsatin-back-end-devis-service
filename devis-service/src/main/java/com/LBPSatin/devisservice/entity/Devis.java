package com.LBPSatin.devisservice.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.LBPSatin.devisservice.enumerates.EtatDevis;
import com.LBPSatin.devisservice.enumerates.TypeGamme;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Devis {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="devisid_generator")
	@SequenceGenerator(name = "devisid_generator",initialValue=1,sequenceName="devisid_seq")
	@Column(name = "id_devis", unique=true, nullable=false)
	private Long idDevis;
	
	@Column(name = "num_devis")
	private Long numDevis;
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_creation")
	private Date dateCreation;
	
	
	@Temporal(TemporalType.DATE)
	@NotNull
	@Column(name = "date_modification")
	private Date dateModification;
	
	@Column(name = "nbr_enfant")
	private int nbrEnfant;
	
	@Column(name = "nbr_adulte")
	private int nbrAdulte;
	
	@Column(name = "etat")
	private EtatDevis etat;
	
	@Column(name = "code_opercom")
	private String codeOpercom;
	
	@Column(name = "type_gamme")
	private TypeGamme typeGamme;
	
	// clé étrangére sur table ContratBrouillon
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_contratBrouillon", referencedColumnName = "id_contratBrouillon")
    private ContratBrouillon contratBrouillon;
}
