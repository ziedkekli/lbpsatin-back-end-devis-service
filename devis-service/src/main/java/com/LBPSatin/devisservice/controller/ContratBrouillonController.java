package com.LBPSatin.devisservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.LBPSatin.devisservice.entity.ContratBrouillon;
import com.LBPSatin.devisservice.servise.ContratBrouillonService;

@RestController
@EnableAutoConfiguration
@CrossOrigin("*")
@RequestMapping(value = "/contratBrouillon")
public class ContratBrouillonController {
	
	@Autowired
	ContratBrouillonService contratBrouillonService;
	
	@PostMapping("/save")
	public ContratBrouillon save(@RequestBody ContratBrouillon contratBrouillon) {
		contratBrouillonService.save(contratBrouillon);
		return contratBrouillon;
	}

}
