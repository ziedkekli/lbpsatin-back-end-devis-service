package com.LBPSatin.devisservice.implServise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.LBPSatin.devisservice.entity.ContratBrouillon;
import com.LBPSatin.devisservice.interfaceRepository.ContratBrouillonRepository;
import com.LBPSatin.devisservice.servise.ContratBrouillonService;

@Service
@Transactional
public class ContratBrouillonImplService implements ContratBrouillonService{
	
	@Autowired
	ContratBrouillonRepository contratBrouillonRepo;
	
	// création d'un contrat brouillon
	@Override
	public void save(ContratBrouillon contratBrouillon) {
		contratBrouillonRepo.save(contratBrouillon);
	}

}
