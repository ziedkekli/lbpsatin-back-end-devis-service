package com.LBPSatin.devisservice.enumerates;

public enum TypeGamme {
	
	/**
     * SENIOR.
     */
    SENIORS("SS"),

    /**
     * FAMILLE_ADULTES.
     */
    FAMILLE_ADULTES("FD"),

    /**
     * JEUNES.
     */
    JEUNES("JE"),
	
	/**
     * ACCESS.
     */
    ACCESS("AC");

    /**
     * code 2 caractères.
     */
    private final String code;

    /**
     * TypeGamme constructor.
     *
     * @param codeGamme sur 2 caractères
     */
    TypeGamme(final String codeGamme) {
        this.code = codeGamme;
    }

    /**
     * Get the value of current Code.
     *
     * @return a String
     */
    public String getCode() {
        return this.code;
    }
}
