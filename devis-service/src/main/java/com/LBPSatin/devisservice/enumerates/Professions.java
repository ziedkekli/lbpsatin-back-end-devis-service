package com.LBPSatin.devisservice.enumerates;

import java.util.ArrayList;
import java.util.List;

public enum Professions {
	

    /**
     * Agriculteurs exploitants.
     */
    AGR_EXPL("Agriculteurs exploitants", "AE"),

    /**
     * Artisans.
     */
    ARTISANS("Artisans", "AR"),

    /**
     * Commerçants et assimilés.
     */
    COMM_ASS("Commerçants et assimilés", "CA"),

    /**
     * Chefs d'entreprise de 10 salariés ou plus.
     */
    CHEFS_EN("Chefs d'entreprise de 10 salariés ou plus", "CE"),

    /**
     * Professions libérales et assimilés.
     */
    PROF_LIB("Professions libérales et assimilés", "PL"),

    /**
     * Cadres de la fonction publique, professions intellectuelles et
     * artistiques.
     */
    CADR_PUB("Cadres de la fonction publique, professions intellectuelles et artistiques", "CF"),

    /**
     * Cadres d'entreprise.
     */
    CADR_ENT("Cadres d'entreprise", "CD"),

    /**
     * Professions intermédiaires de l'enseignement, de la santé, de la fonction
     * publique et assimilés.
     */
    PROF_ENS("Professions intermédiaires de l'enseignement, de la santé, de la fonction publique et assimilés", "PI"),

    /**
     * Professions intermédiaires administratives et commerciales des
     * entreprises.
     */
    PROF_ADM("Professions intermédiaires administratives et commerciales des entreprises", "PA"),

    /**
     * Techniciens.
     */
    TECHNICS("Techniciens", "TH"),

    /**
     * Contremaîtres, agents de maîtrise.
     */
    AGT_MAIT("Contremaîtres, agents de maîtrise", "CR"),

    /**
     * Employés de la fonction publique.
     */
    EMPL_PUB("Employés de la fonction publique", "EP"),

    /**
     * Employés administratifs d'entreprise.
     */
    EMPL_ADM("Employés administratifs d'entreprise", "EA"),

    /**
     * Employés de commerce.
     */
    EMPL_COM("Employés de commerce", "EC"),

    /**
     * Personnels des services directs aux particuliers.
     */
    PERS_SVC("Personnels des services directs aux particuliers", "PP"),

    /**
     * Ouvriers qualifiés.
     */
    OUVR_QLF("Ouvriers qualifiés", "OQ"), OUVR_NQF("Ouvriers non qualifiés", "ON"),

    /**
     * Ouvriers agricoles.
     */
    OUVR_AGR("Ouvriers agricoles", "OA"),

    /**
     * Anciens agriculteurs exploitants.
     */
    OLD_AGRI("Anciens agriculteurs exploitants", "AA"),

    /**
     * Anciens artisans, commerçants, chefs d'entreprise.
     */
    OLD_ARTS("Anciens artisans, commerçants, chefs d'entreprise", "AE"),

    /**
     * Anciens cadres et professions intermédiaires.
     */
    OLD_CADR("Anciens cadres et professions intermédiaires", "AP"),

    /**
     * Anciens employés et ouvriers.
     */
    OLD_EMPL("Anciens employés et ouvriers", "AO"),

    /**
     * Chômeurs n'ayant jamais travaillé.
     */
    CHOM_JTR("Chômeurs n'ayant jamais travaillé", "CJ"),

    /**
     * Inactifs divers (autres que retraités).
     */
    INACTIFS("Inactifs divers (autres que retraités)", "ID");

    /**
     * Profession title.
     */
    private String professionTitle;

    /**
     * Profession base code.
     */
    private String professionBaseCode;

    /**
     * Constructor.
     *
     * @param title : Profession title
     * @param code : Profession base code
     */
    Professions(final String title, final String code) {
        this.setProfessionTitle(title);
        this.setProfessionBaseCode(code);
    }

    /**
     * @return the professionTitle
     */
    public String getProfessionTitle() {
        return this.professionTitle;
    }

    /**
     * @param title the professionTitle to set
     */
    public void setProfessionTitle(final String title) {
        this.professionTitle = title;
    }

    /**
     * @return the professionBaseCode
     */
    public String getProfessionBaseCode() {
        return this.professionBaseCode;
    }

    /**
     * @param code the professionBaseCode to set
     */
    public void setProfessionBaseCode(final String code) {
        this.professionBaseCode = code;
    }

    /**
     * @return the list of professions titles
     */
    public static List<String> getProfessionsTitles() {
        List<String> list = new ArrayList<String>();
        for (Professions profession : Professions.values()) {
            list.add(profession.getProfessionTitle());
        }
        return list;
    }

    /**
     * @return the list of professions codes
     */
    public static List<String> getProfessionsCodes() {
        List<String> list = new ArrayList<String>();
        for (Professions profession : Professions.values()) {
            list.add(profession.getProfessionBaseCode());
        }
        return list;
    }
}
