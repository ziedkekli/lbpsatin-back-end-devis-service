package com.LBPSatin.devisservice.enumerates;

public enum EtatContrat {
	
	/**
     * en cours de création - valeur à passer dans creationPolice.
     */
    EN_COURS_DE_CREATION("EN"),

    /**
     * créé - valeur à passer par le mode ConditionsUtilisation update.
     */
    CREE("CR"),

    /**
     * payé - valeur à passer par le mode Paiement update.
     */
    PAYE("PA"),

    /**
     * signé - valeur à passer par le mode Signature update.
     */
    SIGNE("SG"),

    /**
     * valide/actif - valeur à passer par le ModeEnvoi mode qui est la dernière
     * mise à jour du Contrat lors de la souscription.
     */
    VALIDE("AC");

    /**
     * Code.
     */
    private String code;

    /**
     * Constructeur.
     *
     * @param codeEtat : code
     */
    EtatContrat(final String codeEtat) {
        this.code = codeEtat;
    }

    /**
     * Accesseur de code.
     *
     * @return La valeur de <code>code</code>
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Récupératin de l'état en fonction de son code.
     *
     * @param codeEtat : code
     * @return état du contrat
     */
    public static EtatContrat valueOfCode(final String codeEtat) {
        for (EtatContrat ec : values()) {
            if (ec.getCode().equalsIgnoreCase(codeEtat)) {
                return ec;
            }
        }
        return null;
    }
}
